// (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:module_ref:conn:1.0
// IP Revision: 1

(* X_CORE_INFO = "conn,Vivado 2017.4" *)
(* CHECK_LICENSE_TYPE = "base_block_design_conn_0_0,conn,{}" *)
(* CORE_GENERATION_INFO = "base_block_design_conn_0_0,conn,{x_ipProduct=Vivado 2017.4,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=conn,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module base_block_design_conn_0_0 (
  clk,
  gpio0,
  gpio1,
  gpio2,
  gpio3,
  gpio4,
  comm_sce,
  comm_dc,
  comm_res,
  comm_sclk,
  comm_mosi
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME core_clk, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN base_block_design_processing_system7_0_0_FCLK_CLK0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 core_clk CLK" *)
input wire clk;
inout wire [24 : 0] gpio0;
inout wire [24 : 0] gpio1;
inout wire [24 : 0] gpio2;
inout wire [24 : 0] gpio3;
inout wire [24 : 0] gpio4;
input wire comm_sce;
input wire comm_dc;
input wire comm_res;
input wire comm_sclk;
input wire comm_mosi;

  conn inst (
    .clk(clk),
    .gpio0(gpio0),
    .gpio1(gpio1),
    .gpio2(gpio2),
    .gpio3(gpio3),
    .gpio4(gpio4),
    .comm_sce(comm_sce),
    .comm_dc(comm_dc),
    .comm_res(comm_res),
    .comm_sclk(comm_sclk),
    .comm_mosi(comm_mosi)
  );
endmodule
