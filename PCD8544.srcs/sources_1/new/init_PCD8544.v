module init_PCD8544 #(
        CLK_FREQ = 50000000// : CLK_FREQ > 0 // clock frequency
    )(
        input clk,    // clock
        input rst,    // reset

        input wr_full,
        output reg wr_valid,
        output reg [8:0] wr_data,

        output reg done
    );

    localparam STATE_SET_HOME = 0;
    localparam STATE_CLEAR_SCRN = 1;
    localparam STATE_SETTINGS = 2;
    localparam STATE_WHITE_SCRN = 3;
    localparam STATE_WAIT1 = 4;
    localparam STATE_BLACK_SCRN = 5;
    localparam STATE_WAIT2 = 6;
    localparam STATE_NORMAL_SCRN = 7;
    localparam STATE_SCRN_DATA = 8;
    localparam STATE_DONE = 9;

    // true registers
    reg [8:0] cmdctr = 0;
    reg [$clog2(CLK_FREQ/4+1)-1:0] timectr = 0;
    reg [3:0] state = STATE_SET_HOME;

    // combonational registers
    reg [8:0] cmdctr_d;
    reg [$clog2(CLK_FREQ/4+1)-1:0] timectr_d;
    reg [3:0] state_d;

    always @(posedge clk or posedge rst)
        if (rst == 1)
        begin
            cmdctr <= 0;
            timectr <= 0;
            state <= STATE_SET_HOME;
        end
        else
        begin
            cmdctr <= cmdctr_d;
            timectr <= timectr_d;
            state <= state_d;
        end

    always @*
    begin
        cmdctr_d = 0;
        timectr_d = 0;
        done = 0;
        wr_valid = 0;
        wr_data = 9'bx;

        state_d = state;

        // laststate_d = state;
        if(state == STATE_DONE)
        begin
            done = 1;
        end
        else
        begin
            if(!wr_full)
            begin
                case(state)
                    STATE_SET_HOME:
                    begin
                        cmdctr_d = cmdctr + 1;
                        if(cmdctr == 0)
                            wr_data = 9'h020; // Normal commands
                        if(cmdctr == 1)
                            wr_data = 9'h080; // X => 0
                        if(cmdctr == 2)
                            wr_data = 9'h040; // Y => 0
                        if(cmdctr < 3)
                            wr_valid = 1;
                        if(cmdctr == 3)
                        begin
                            cmdctr_d = 0;
                            state_d = STATE_CLEAR_SCRN;
                        end

                    end
                    STATE_CLEAR_SCRN:
                    begin
                        cmdctr_d = cmdctr + 1;
                        if(cmdctr < 504)
                        begin
                            wr_data = {1'b1, 8'h00};
                            wr_valid = 1;
                        end
                        else
                        begin
                            cmdctr_d = 0;
                            state_d = STATE_SETTINGS;
                        end

                    end
                    STATE_SETTINGS:
                    begin
                        cmdctr_d = cmdctr + 1;
                        if(cmdctr == 0)
                            wr_data = 9'h021; // Extended commands
                        if(cmdctr == 1)
                            wr_data = 9'h0C8; // Set contrast (voltage bias)
                        if(cmdctr == 2)
                            wr_data = 9'h004; // Temperature coefficient
                        if(cmdctr == 3)
                            wr_data = 9'h013; // Bias mode 1:48
                        if(cmdctr == 4)
                            wr_data = 9'h020; // Normal commands
                        if(cmdctr < 5)
                            wr_valid = 1;
                        if(cmdctr == 5)
                        begin
                            cmdctr_d = 0;
                            state_d = STATE_WHITE_SCRN;
                        end

                    end
                    STATE_WHITE_SCRN:
                    begin
                        wr_data = 9'h008; // White screen
                        wr_valid = 1;
                        state_d = STATE_WAIT1;

                    end
                    STATE_WAIT1:
                    begin
                        timectr_d = timectr + 1;
                        if(timectr == CLK_FREQ/4)
                        begin
                            timectr_d = 0;
                            state_d = STATE_BLACK_SCRN;
                        end

                    end
                    STATE_BLACK_SCRN:
                    begin
                        wr_data = 9'h009; // Black screen
                        wr_valid = 1;
                        state_d = STATE_WAIT2;

                    end
                    STATE_WAIT2:
                    begin
                        timectr_d = timectr + 1;
                        if(timectr == CLK_FREQ/4)
                        begin
                            timectr_d = 0;
                            state_d = STATE_NORMAL_SCRN;
                        end

                    end
                    STATE_NORMAL_SCRN:
                    begin
                        wr_data = 9'h00C; // Normal screen
                        wr_valid = 1;
                        state_d = STATE_DONE;

                    end
                    STATE_SCRN_DATA:
                    begin
                        cmdctr_d = cmdctr + 1;
                        if(cmdctr < 504)
                        begin
                            wr_data = {1'b1, cmdctr[7:0]};//8'h00};
                            wr_valid = 1;
                        end
                        else
                        begin
                            cmdctr_d = 0;
                            state_d = STATE_DONE;
                        end
                    end
                endcase
            end
            else
            begin
                cmdctr_d = cmdctr;
            end
        end
    end
endmodule