`timescale 1ns / 1ps

module comm_PCD8544 #(
        BAUD_CYCLES = 50
    )(
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 core_clk CLK" *)
        input clk,           // clock
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME core_rst, POLARITY ACTIVE_HIGH" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 core_rst RST" *)
        input rst,           // reset

        input rd_valid,      // data good from fifo
        (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 DIN EMPTY" *)
        input rd_empty,      // empty signal from fifo
        (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 DIN RD_DATA" *)
        input [8:0] rd_data, // data from fifo
        (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_read:1.0 DIN RD_EN" *)
        output reg rd_ready, // command to signal fifo we want to read


        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 M_PCD EN" *)
        output reg sce,      // !SCE - Serial Enable
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 M_PCD DC" *)
        output reg dc,       // D/!C - data or command
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 M_PCD RST" *)
        output reg res,      // !RES - reset to inturrupt current transfer
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 M_PCD CLK" *)
        output reg sclk,     // SCLK - clock
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 M_PCD DATA" *)
        output reg mosi      // SDIN - serial data (changes on falling edge of SCLK)
    );

    localparam STATE_INIT = 0;
    localparam STATE_IDLE = 1;
    localparam STATE_READ = 2;
    localparam STATE_SCE = 3;
    localparam STATE_SEND = 4;

    // true registers
    reg [$clog2(BAUD_CYCLES+1)-1:0] baudctr = 0;
    reg sclkval = 1;
    reg sceval = 1;
    reg [8:0] databuf;
    reg [3:0] bitctr;
    reg [2:0] state = STATE_INIT;
    reg [2:0] laststate = STATE_INIT;

    // combonational registers
    reg [$clog2(BAUD_CYCLES+1)-1:0] baudctr_d;
    reg sclkval_d;
    reg sceval_d;
    reg [8:0] databuf_d;
    reg [3:0] bitctr_d;
    reg [2:0] state_d;
    reg [2:0] laststate_d;

    always @(posedge clk or posedge rst)
        if (rst == 1)
        begin
            baudctr <= 0;
            bitctr <= 0;
            databuf <= 0;
            sclkval <= 1;
            sceval <= 1;
            state <= STATE_INIT;
            laststate <= STATE_INIT;
        end
        else
        begin
            baudctr <= baudctr_d;
            bitctr <= bitctr_d;
            databuf <= databuf_d;
            sclkval <= sclkval_d;
            sceval <= sceval_d;
            state <= state_d;
            laststate <= laststate_d;
        end

    always @*
    begin
        baudctr_d = baudctr + 1;
        bitctr_d = 0;
        sceval_d = sceval;
        sclkval_d = sclkval;
        databuf_d = databuf;

        rd_ready = 0;

        res = 1; // high is working
        sce = sceval; // low is "care about clock pulses"
        dc = 0; // low is command, high is data
        sclk = sclkval;
        mosi = databuf[7];

        state_d = state;
        laststate_d = state;

        case (state)
            STATE_INIT:
            begin
                if (baudctr < BAUD_CYCLES / 2)
                    res = 0;
                if (baudctr == BAUD_CYCLES)
                    state_d = STATE_IDLE;
            end

            STATE_IDLE:
            begin
                baudctr_d = 0;
                sceval_d = 1;

                if (!rd_empty)
                    state_d = STATE_READ;
            end

            STATE_READ:
            begin
                baudctr_d = 0;
                if (laststate != state) // only set rd_ready once
                    rd_ready = 1;

                if (rd_valid)
                begin
                    if (sceval)
                        state_d = STATE_SCE;
                    else
                        state_d = STATE_SEND;

                    databuf_d = rd_data;
                end
            end

            STATE_SCE:
            begin
                sceval_d = 0;
                if (baudctr == BAUD_CYCLES)
                begin
                    baudctr_d = 0;
                    state_d = STATE_SEND;
                end
            end

            STATE_SEND:
            begin
                sclk = sclkval;
                // mosi = databuf[7];
                dc = databuf[8];
                bitctr_d = bitctr;

                if (laststate != state)
                    sclkval_d = 0;

                if (baudctr == BAUD_CYCLES)
                begin
                    sclkval_d = !sclkval;
                    baudctr_d = 0;
                    if (sclkval == 1) // if this is a falling edge, update data
                    begin
                        databuf_d = {databuf[8], databuf[6:0], 1'b0};
                        bitctr_d = bitctr + 1;

                        if (bitctr == 7)
                        begin
                            sclkval_d = 1;
                            if (!rd_empty)
                                state_d = STATE_READ;
                            else
                                state_d = STATE_IDLE;
                        end
                    end
                end
            end
        endcase
    end
endmodule
