`timescale 1 ns / 1 ps

module axi_lite #(
        parameter integer REG_DEPTH = 4,
        parameter integer REG_WIDTH = 32,

        // DO NOT MODIFY
        parameter integer REG_BYTES = REG_WIDTH / 8, // number of bytes in a register
        parameter integer REG_DBITS = $clog2(REG_BYTES), // number of bits required to address a byte within a register
        parameter integer REG_ABITS = $clog2(REG_DEPTH), // number of bits required to address a register
        parameter integer AXI_ABITS = REG_ABITS + REG_DBITS // number of bits required for the AXI bus
    )(
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_aclk, ASSOCIATED_BUSIF S_AXI, ASSOCIATED_RESET s_axi_aresetn" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s_axi_aclk CLK" *)
        input wire s_axi_aclk, // slave AXI clock

        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_aresetn, POLARITY ACTIVE_LOW" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s_axi_aresetn RST" *)
        input wire s_axi_aresetn, // slave AXI reset (active-low)

        // slave AXI-Lite write channel
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWADDR"  *) input wire [AXI_ABITS-1:0] s_axi_awaddr,  // address
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWPROT"  *) input wire [2:0]           s_axi_awprot,  // channel protection type
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWVALID" *) input wire                 s_axi_awvalid, // address valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWREADY" *) output reg                 s_axi_awready, // address ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WDATA"   *) input wire [REG_WIDTH-1:0] s_axi_wdata,   // data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WSTRB"   *) input wire [REG_BYTES-1:0] s_axi_wstrb,   // strobes - one bit per byte of data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WVALID"  *) input wire                 s_axi_wvalid,  // data/strobes valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WREADY"  *) output reg                 s_axi_wready,  // data/strobes ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI BRESP"   *) output reg [1:0]           s_axi_bresp,   // response
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI BVALID"  *) output reg                 s_axi_bvalid,  // response valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI BREADY"  *) input wire                 s_axi_bready,  // response ready

        // slave AXI-Lite read channel
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARADDR"  *) input wire [AXI_ABITS-1:0] s_axi_araddr,  // address
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARPROT"  *) input wire [2:0]           s_axi_arprot,  // channel protection type
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARVALID" *) input wire                 s_axi_arvalid, // address valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARREADY" *) output reg                 s_axi_arready, // address ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RDATA"   *) output reg [REG_WIDTH-1:0] s_axi_rdata,   // data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RRESP"   *) output reg [1:0]           s_axi_rresp,   // response
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RVALID"  *) output reg                 s_axi_rvalid,  // data/response valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RREADY"  *) input wire                 s_axi_rready,  // data/response ready

        // register interface
        output wire                 reg_ren,   // read enable
        output reg  [REG_ABITS-1:0] reg_raddr, // read address
        input  wire [REG_WIDTH-1:0] reg_rdata, // read data
        output wire                 reg_wen,   // write enable
        output reg  [REG_ABITS-1:0] reg_waddr, // write address
        output reg  [REG_WIDTH-1:0] reg_wdata, // write data
        input  wire [REG_WIDTH-1:0] reg_wfdbk  // write feedback of registers to support strobing
    );

    /*
    * # AXI4 Lite
    *
    * Read/write channel protection type indigates the priviledge and security
    * level of the transaction, and whether it is a data access or instruction
    * access.
    *
    * AWADDR - in, write address
    * AWVALID - in, write address is valid
    * AWREADY - out, ready for write address
    * WDATA - in, write data
    * WSTRB - in, write strobe lines
    * WVALID - in, write data is valid
    * WREADY - out, ready for write data
    * BRESP - out, write response
    * BVALID - out, write response is valid
    * BREADY - in, ready for write response
    *
    * ARADDR - in, read address
    * ARVALID - in, read address is valid
    * ARREADY - out, ready for read address
    * RDATA - out, read data
    * RRESP - out, read response
    * RVALID - out, read data and response is valid
    * RREADY - in, ready for read data and response
    */

    // internal signals
    reg aw_en; // enable AXI write address receive
    integer index; // byte index for strobing

    assign reg_wen = s_axi_wready && s_axi_wvalid && s_axi_awready && s_axi_awvalid;
    assign reg_ren = s_axi_arready && s_axi_arvalid && ~s_axi_rvalid;

    // write data processing;
    //   assert register write enable and output data on register write data
    //   lines when write address and data are valid and ready;
    //   write strobes are used to select particular bytes to write.
    always @* begin
        reg_wdata = 0;

        if (s_axi_aresetn && reg_wen) begin
            for (index = 0; index < REG_DBITS; index = index + 1) begin
                reg_wdata[index*8 +: 8] = s_axi_wstrb[index]
                    ? s_axi_wdata[index*8 +: 8]
                    : reg_wfdbk[index*8 +: 8];
            end
        end
    end

    always @(posedge s_axi_aclk) begin
        if (~s_axi_aresetn) begin
            s_axi_awready <= 0;
            s_axi_wready <= 0;
            s_axi_bvalid <= 0;
            s_axi_bresp <= 0;
            s_axi_arready <= 0;
            s_axi_rvalid <= 0;
            s_axi_rresp <= 0;
            s_axi_rdata <= 0;

            aw_en <= 1;

            reg_waddr <= 0;
            reg_raddr <= 0;
        end
        else begin
        end

        // write address ready generation and latching;
        //   `s_axi_awready` is asserted for one clock cycle when both
        //   `s_axi_awvalid` and `s_axi_wvalid` are asserted. `s_axi_awready` is
        //   deasserted when reset is active.
        if (~s_axi_awready && s_axi_awvalid && s_axi_wvalid && aw_en) begin
            // Slave is ready to accept write address when there is a valid
            // write address and write data on the write address and data
            // bus. This design expects no outstanding transactions.
            s_axi_awready <= 1;
            aw_en <= 0;
            reg_waddr <= s_axi_awaddr[REG_DBITS +: REG_ABITS];
        end
        else if (s_axi_bready && s_axi_bvalid) begin
            aw_en <= 1;
            s_axi_awready <= 0;
        end
        else begin
            s_axi_awready <= 0;
        end

        // write data ready generation;
        //   `s_axi_wready` is asserted for one clock cycle when both
        //   `s_axi_awvalid` and `s_axi_wvalid` are asserted. `s_axi_wready` is
        //   deasserted when reset is active
        if (~s_axi_wready && s_axi_awvalid && s_axi_wvalid && aw_en) begin
            // Slave is ready to accept write data when there is a valid write
            // address and write data on the write address and data bus. This
            // design expects no outstanding transactions.
            s_axi_wready <= 1;
        end
        else begin
            s_axi_wready <= 0;
        end

        // write response generation;
        //   `s_axi_bresp` and `s_axi_bvalid` are asserted when `s_axi_awready`,
        //   `s_axi_awvalid`, `s_axi_wready`, and `s_axi_wvalid` are asserted. This
        //   marks the acceptance of the address and indicates the status of write
        //   transaction.
        if (s_axi_awready && s_axi_awvalid && ~s_axi_bvalid && s_axi_wready && s_axi_wvalid) begin
            s_axi_bvalid <= 1; // write response is available at the write response bus
            s_axi_bresp <= 0; // 'OKAY' response
        end
        else if (s_axi_bready && s_axi_bvalid) begin
            // check if bready is asserted while bvalid is high
            // (there is a possibility that bready is always asserted high)
            s_axi_bvalid <= 0;
        end

        // read address ready generation and latching;
        //   `s_axi_arready` is asserted for one clock cycle when `s_axi_arvalid`
        //   is asserted. `s_axi_arready` is deasserted when reset is active.
        //   The read address is also latched (and reset on reset).
        if (~s_axi_arready && s_axi_arvalid) begin
            s_axi_arready <= 1; // read address is accepted
            reg_raddr <= s_axi_araddr[REG_DBITS +: REG_ABITS]; // latch read address
        end
        else begin
            s_axi_arready <= 0;
        end

        // read data valid generation; CLEANUP
        // axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both
        // S_AXI_ARVALID and axi_arready are asserted. The slave registers
        // data are available on the axi_rdata bus at this instance. The
        // assertion of axi_rvalid marks the validity of read data on the
        // bus and axi_rresp indicates the status of read transaction.axi_rvalid
        // is deasserted on reset (active low). axi_rresp and axi_rdata are
        // cleared to zero on reset (active low).
        if (s_axi_arready && s_axi_arvalid && ~s_axi_rvalid) begin
            s_axi_rvalid <= 1; // read data is available at the read data bus
            s_axi_rresp <= 0; // 'OKAY' response
        end
        else if (s_axi_rvalid && s_axi_rready) begin
            // read data is accepted by the master
            s_axi_rvalid <= 0;
        end

        // read data processing
        if (reg_ren) begin
            // When there is a valid read address (s_axi_arvalid) with
            // acceptance of the read address by the slave (s_axi_arready),
            // output the read data
            s_axi_rdata <= reg_rdata;
        end
    end

endmodule