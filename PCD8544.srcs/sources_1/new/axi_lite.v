`timescale 1 ns / 1 ps

module axi_lite #(
        parameter integer REG_DEPTH = 4,
        parameter integer REG_WIDTH = 32,

        // DO NOT MODIFY
        parameter integer REG_BYTES = REG_WIDTH / 8, // number of bytes in a register
        parameter integer REG_ABITS = $clog2(REG_DEPTH), // number of bits required to address a register
        parameter integer REG_DBITS = $clog2(REG_BYTES), // number of bits required to address the bytes of a register
        parameter integer AXI_ABITS = REG_ABITS + REG_DBITS // number of bits required for the AXI bus
    )(
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_aclk, ASSOCIATED_BUSIF S_AXI, ASSOCIATED_RESET s_axi_aresetn" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s_axi_aclk CLK" *)
        input wire s_axi_aclk, // slave AXI clock

        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_aresetn, POLARITY ACTIVE_LOW" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s_axi_aresetn RST" *)
        input wire s_axi_aresetn, // slave AXI reset (active-low)

        // slave AXI-Lite write channel
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWADDR"  *) input wire [AXI_ABITS-1:0] s_axi_awaddr,  // address
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWPROT"  *) input wire [2:0]           s_axi_awprot,  // channel protection type
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWVALID" *) input wire                 s_axi_awvalid, // address valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI AWREADY" *) output reg                 s_axi_awready, // address ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WDATA"   *) input wire [REG_WIDTH-1:0] s_axi_wdata,   // data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WSTRB"   *) input wire [REG_BYTES-1:0] s_axi_wstrb,   // strobes - one bit per byte of data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WVALID"  *) input wire                 s_axi_wvalid,  // data/strobes valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI WREADY"  *) output reg                 s_axi_wready,  // data/strobes ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI BRESP"   *) output reg [1:0]           s_axi_bresp,   // response
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI BVALID"  *) output reg                 s_axi_bvalid,  // response valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI BREADY"  *) input wire                 s_axi_bready,  // response ready

        // slave AXI-Lite read channel
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARADDR"  *) input wire [AXI_ABITS-1:0] s_axi_araddr,  // address
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARPROT"  *) input wire [2:0]           s_axi_arprot,  // channel protection type
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARVALID" *) input wire                 s_axi_arvalid, // address valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI ARREADY" *) output reg                 s_axi_arready, // address ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RDATA"   *) output reg [REG_WIDTH-1:0] s_axi_rdata,   // data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RRESP"   *) output reg [1:0]           s_axi_rresp,   // response
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RVALID"  *) output reg                 s_axi_rvalid,  // data/response valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI RREADY"  *) input wire                 s_axi_rready,  // data/response ready

        // register interface
        output reg                 reg_rready,
        input wire                 reg_rvalid,
        output reg [REG_ABITS-1:0] reg_raddr,
        input wire [REG_WIDTH-1:0] reg_rdata,

        output reg                 reg_wvalid,
        input wire                 reg_wready,
        output reg [REG_ABITS-1:0] reg_waddr,
        output reg [REG_WIDTH-1:0] reg_wdata,
        output reg [REG_BYTES-1:0] reg_wstrb
    );



    localparam WS_IDLE = 0; // idle
    localparam WS_DATA = 1; // waiting for data
    localparam WS_RESP = 2; // waiting for response

    reg [1:0] wstate = WS_IDLE;

    initial begin
        // AXI: ready for address and data, response is not valid
        s_axi_awready <= 1;
        s_axi_wready <= 1;
        s_axi_bresp <= 0;
        s_axi_bvalid <= 0;

        // REG: nothing is valid
        reg_wvalid <= 0;
        reg_waddr <= 0;
        reg_wdata <= 0;
        reg_wstrb <= 0;
    end

    always @(posedge s_axi_aclk) begin
        if (~s_axi_aresetn) begin
            wstate <= WS_IDLE;

            // AXI: ready for address and data, response is not valid
            s_axi_awready <= 1;
            s_axi_wready <= 1;
            s_axi_bresp <= 0;
            s_axi_bvalid <= 0;

            // REG: nothing is valid
            reg_wvalid <= 0;
            reg_waddr <= 0;
            reg_wdata <= 0;
            reg_wstrb <= 0;
        end
        else begin
            case (wstate)
                WS_IDLE: begin
                    // AXI: wait for valid address
                    if (s_axi_awvalid && s_axi_awready) begin
                        // AXI=>REG: latch address
                        reg_waddr = s_axi_awaddr[REG_DBITS +: REG_ABITS];

                        // AXI: deassert ready for address
                        s_axi_awready = 0;
                    end

                    // AXI: wait for valid data
                    if (s_axi_wvalid && s_axi_wready) begin
                        // AXI=>REG: latch data and strobes
                        reg_wdata = s_axi_wdata;
                        reg_wstrb = s_axi_wstrb;

                        // AXI: deassert ready for data
                        s_axi_wready = 0;
                    end

                    // once both are done
                    if (~s_axi_awready && ~s_axi_wready) begin
                        // AXI: send response
                        s_axi_bresp = 0; // OKAY

                        // AXI: assert response is valid
                        s_axi_bvalid = 1;

                        // REG: assert data is valid
                        reg_wvalid = 1;

                        // continue
                        wstate = WS_RESP;
                    end
                end

                WS_DATA: begin
                end

                WS_RESP: begin
                    // AXI: wait for ready for response
                    if (s_axi_bready && s_axi_bvalid) begin
                        // AXI: deassert response is valid
                        s_axi_bvalid = 0;

                        // AXI: assert ready for address
                        s_axi_awready = 1;

                        // AXI: assert ready for data
                        s_axi_wready = 1;
                    end

                    // REG: wait for ready for data
                    if (reg_wready && reg_wvalid) begin
                        // REG: deassert data is valid
                        reg_wvalid = 0;
                    end

                    // once both are done
                    if (~s_axi_bvalid && ~reg_wvalid) begin
                        // restart
                        wstate = WS_IDLE;
                    end
                end
            endcase
        end
    end



    localparam RS_IDLE = 0; // idle
    localparam RS_DATA = 1; // waiting for data
    localparam RS_RESP = 2; // waiting for response

    reg [1:0] rstate = RS_IDLE;

    always @(posedge s_axi_aclk) begin
        if (~s_axi_aresetn) begin
            rstate <= RS_IDLE;

            s_axi_arready <= 0;
            s_axi_rdata <= 0;
            s_axi_rresp <= 0;
            s_axi_rvalid <= 0;

            reg_rready <= 0;
            reg_raddr <= 0;
        end
        else begin
            case (rstate)
                RS_IDLE: begin
                    // AXI: assert ready for address
                    s_axi_arready <= 1;

                    // AXI: wait for address
                    if (s_axi_arvalid) begin
                        // AXI=>REG: latch address
                        reg_raddr <= s_axi_araddr[REG_DBITS +: REG_ABITS];

                        // REG: assert ready for data
                        reg_rready <= 1;

                        // continue
                        rstate <= RS_DATA;
                    end
                end

                RS_DATA: begin
                    // AXI: deassert ready for address
                    s_axi_arready <= 0;

                    // REG: wait for data
                    if (reg_rvalid) begin
                        // REG: deassert ready for data
                        reg_rready <= 0;

                        // REG=>AXI: latch data
                        s_axi_rdata <= reg_rdata;

                        // AXI: send response
                        s_axi_rresp <= 0; // OKAY

                        // AXI: assert response is valid
                        s_axi_rvalid <= 1;

                        // continue
                        rstate <= RS_RESP;
                    end
                end

                RS_RESP: begin
                    // AXI: wait for response accepted
                    if (s_axi_rready) begin
                        // AXI: deassert response is valid
                        s_axi_rvalid <= 0;

                        // restart
                        rstate <= RS_IDLE;
                    end
                end
            endcase
        end
    end

endmodule