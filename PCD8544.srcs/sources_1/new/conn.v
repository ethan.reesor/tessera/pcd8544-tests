`timescale 1ns / 1ps

module conn(
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 core_clk CLK" *)
        input clk,

        inout [24:0] gpio0,
        inout [24:0] gpio1,
        inout [24:0] gpio2,
        inout [24:0] gpio3,
        inout [24:0] gpio4,

        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 S_PCD EN" *)
        input comm_sce,         // !SCE - Serial Enable
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 S_PCD DC" *)
        input comm_dc,          // D/!C - data or command
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 S_PCD RST" *)
        input comm_res,         // !RES - reset to inturrupt current transfer
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 S_PCD CLK" *)
        input comm_sclk,        // SCLK - clock
        (* X_INTERFACE_INFO = "firelizzard.com:interface:pcd8544:1.0 S_PCD DATA" *)
        input comm_mosi         // SDIN - serial data (changes on falling edge of SCLK)
    );

    reg [24:0] gpio0_d;
    reg [24:0] gpio1_d;
    reg [24:0] gpio2_d;
    reg [24:0] gpio3_d;
    reg [24:0] gpio4_d;

    assign gpio0 = gpio0_d;
    assign gpio1 = gpio1_d;
    assign gpio2 = gpio2_d;
    assign gpio3 = gpio3_d;
    assign gpio4 = gpio4_d;

    always @*
    begin
        gpio0_d = {25{1'bz}};
        gpio1_d = {25{1'bz}};
        gpio2_d = {25{1'bz}};
        gpio3_d = {25{1'bz}};
        gpio4_d = {25{1'bz}};

        // gpio0[0] = re ? 'bz : count[23];

        gpio0_d[19] = comm_sce; // JA1.25
        gpio0_d[2] = comm_res;  // JA1.29
        gpio0_d[3] = comm_dc;   // JA1.31
        gpio0_d[6] = comm_mosi; // JA1.35
        gpio0_d[7] = comm_sclk; // JA1.37
    end

endmodule
