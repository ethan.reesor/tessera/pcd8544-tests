`timescale 1ns / 1ps

module main #(
        CLK_FREQ = 50000000// : CLK_FREQ > 0 // clock frequency
    )(
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME core_clk, ASSOCIATED_RESET core_rst, ASSOCIATED_BUSIF S_AXI_REG:M_AXIS_CMD:S_AXIS_STS" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 core_clk CLK" *)
        input clk,
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME data_clk, ASSOCIATED_RESET core_rst, ASSOCIATED_BUSIF S_AXIS_DATA" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 data_clk CLK" *)
        input data_clk,
        (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME core_rst, POLARITY ACTIVE_HIGH" *)
        (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 core_rst RST" *)
        input rst,

        (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 DOUT FULL" *)
        input wr_full,
        (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 DOUT WR_DATA" *)
        output reg [8:0] wr_data,
        (* X_INTERFACE_INFO = "xilinx.com:interface:fifo_write:1.0 DOUT WR_EN" *)
        output reg wr_valid,

        // slave AXI-Lite write channel FROM PS
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG AWADDR"  *) input  wire [5:0]  s_axi_reg_awaddr,  // address
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG AWPROT"  *) input  wire [2:0]  s_axi_reg_awprot,  // channel protection type
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG AWVALID" *) input  wire        s_axi_reg_awvalid, // address valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG AWREADY" *) output wire        s_axi_reg_awready, // address ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG WDATA"   *) input  wire [31:0] s_axi_reg_wdata,   // data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG WSTRB"   *) input  wire [3:0]  s_axi_reg_wstrb,   // strobes - one bit per byte of data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG WVALID"  *) input  wire        s_axi_reg_wvalid,  // data/strobes valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG WREADY"  *) output wire        s_axi_reg_wready,  // data/strobes ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG BRESP"   *) output wire [1:0]  s_axi_reg_bresp,   // response
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG BVALID"  *) output wire        s_axi_reg_bvalid,  // response valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG BREADY"  *) input  wire        s_axi_reg_bready,  // response ready

        // slave AXI-Lite read channel FROM PS
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG ARADDR"  *) input  wire [5:0]  s_axi_reg_araddr,  // address
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG ARPROT"  *) input  wire [2:0]  s_axi_reg_arprot,  // channel protection type
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG ARVALID" *) input  wire        s_axi_reg_arvalid, // address valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG ARREADY" *) output wire        s_axi_reg_arready, // address ready
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG RDATA"   *) output wire [31:0] s_axi_reg_rdata,   // data
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG RRESP"   *) output wire [1:0]  s_axi_reg_rresp,   // response
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG RVALID"  *) output wire        s_axi_reg_rvalid,  // data/response valid
        (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_REG RREADY"  *) input  wire        s_axi_reg_rready,  // data/response ready

        // master AXI-Stream command channel TO DataMover
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_CMD TDATA"   *) output wire [71:0] m_axis_cmd_tdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_CMD TREADY"  *) output wire        m_axis_cmd_tvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_CMD TVALID"  *) input  wire        m_axis_cmd_tready,

        // slave AXI-Stream status channel FROM DataMover
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TDATA"   *) input  wire [7:0]  s_axis_sts_tdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TKEEP"   *) input  wire [0:0]  s_axis_sts_tkeep,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TLAST"   *) input  wire        s_axis_sts_tlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TREADY"  *) input  wire        s_axis_sts_tvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TVALID"  *) output wire        s_axis_sts_tready,

        // slave AXI-Stream data channel FROM DataMover
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TDATA"  *) input  wire [63:0] s_axis_data_tdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TKEEP"  *) input  wire [7:0]  s_axis_data_tkeep,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TLAST"  *) input  wire        s_axis_data_tlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TREADY" *) input  wire        s_axis_data_tvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TVALID" *) output wire        s_axis_data_tready,

        // DataMover memory-mapped to stream error
        input wire datamover_mm2s_err
    );

    // registers
    reg enable_driver;
    reg [1:0] fb_valid;
    reg [31:0] fb0_addr;
    reg [31:0] fb1_addr;
    wire [7:0] last_dm_status;
    wire [0:0] current_fb;
    wire [4:0] driver_state;

    // display
    wire init_done;
    wire init_wr_valid;
    wire [8:0] init_wr_data;
    wire disp_wr_valid;
    wire [8:0] disp_wr_data;

    always @*
    begin
        if (!init_done)
        begin
            wr_data = init_wr_data;
            wr_valid = init_wr_valid;
        end
        else
        begin
            wr_data = disp_wr_data;
            wr_valid = disp_wr_valid;
        end
    end

    // register interface
    wire        reg_rready;
    reg         reg_rvalid;
    wire [3:0]  reg_raddr;
    reg  [31:0] reg_rdata;
    wire        reg_wvalid;
    reg         reg_wready;
    wire [3:0]  reg_waddr;
    wire [31:0] reg_wdata;
    wire [3:0]  reg_wstrb;

    reg  [31:0] old_wdata;
    reg  [31:0] new_wdata;

    always @* begin
        case (reg_raddr)
            // status
            4'h0: reg_rdata <= last_dm_status;
            4'h1: reg_rdata <= current_fb;
            4'h2: reg_rdata <= datamover_mm2s_err;
            4'h3: reg_rdata <= init_done;
            4'h4: reg_rdata <= driver_state;

            // control
            4'h8: reg_rdata <= enable_driver;
            4'h9: reg_rdata <= fb_valid;

            // addresses
            4'hC: reg_rdata <= fb0_addr;
            4'hD: reg_rdata <= fb1_addr;

            default: reg_rdata <= 0;
        endcase
    end

    integer index;
    always @* begin
        old_wdata = 0;
        new_wdata = 0;

        if (reg_wvalid) begin
            case (reg_waddr)
                4'h8: old_wdata = enable_driver;
                4'h9: old_wdata = fb_valid;

                4'hC: old_wdata = fb0_addr;
                4'hD: old_wdata = fb1_addr;
            endcase

            for (index = 0; index < 4; index = index + 1) begin
                new_wdata[index*8 +: 8] = reg_wstrb[index]
                    ? reg_wdata[index*8 +: 8]
                    : old_wdata[index*8 +: 8];
            end
        end
    end

    always @(posedge clk) begin
        if (reg_wvalid) case (reg_waddr)
            4'h8: enable_driver = new_wdata;
            4'h9: fb_valid = new_wdata;

            4'hC: fb0_addr = new_wdata;
            4'hD: fb1_addr = new_wdata;
        endcase
    end

    // modules
    axi_lite #(
        .REG_WIDTH(32),
        .REG_DEPTH(16)
    ) reg_axi (
        .s_axi_aclk(clk),
        .s_axi_aresetn(~rst),
        .s_axi_awaddr(s_axi_reg_awaddr),
        .s_axi_awprot(s_axi_reg_awprot),
        .s_axi_awvalid(s_axi_reg_awvalid),
        .s_axi_awready(s_axi_reg_awready),
        .s_axi_wdata(s_axi_reg_wdata),
        .s_axi_wstrb(s_axi_reg_wstrb),
        .s_axi_wvalid(s_axi_reg_wvalid),
        .s_axi_wready(s_axi_reg_wready),
        .s_axi_bresp(s_axi_reg_bresp),
        .s_axi_bvalid(s_axi_reg_bvalid),
        .s_axi_bready(s_axi_reg_bready),
        .s_axi_araddr(s_axi_reg_araddr),
        .s_axi_arprot(s_axi_reg_arprot),
        .s_axi_arvalid(s_axi_reg_arvalid),
        .s_axi_arready(s_axi_reg_arready),
        .s_axi_rdata(s_axi_reg_rdata),
        .s_axi_rresp(s_axi_reg_rresp),
        .s_axi_rvalid(s_axi_reg_rvalid),
        .s_axi_rready(s_axi_reg_rready),
        .reg_rready(reg_rready),
        .reg_rvalid(1),
        .reg_raddr(reg_raddr),
        .reg_rdata(reg_rdata),
        .reg_wvalid(reg_wvalid),
        .reg_wready(1),
        .reg_waddr(reg_waddr),
        .reg_wdata(reg_wdata),
        .reg_wstrb(reg_wstrb)
    );

    init_PCD8544 #(
        .CLK_FREQ(CLK_FREQ)
    ) init (
        .clk(clk),
        .rst(rst),
        .wr_full(wr_full),
        .wr_valid(init_wr_valid),
        .wr_data(init_wr_data),
        .done(init_done)
    );

    driver #(
        .FB_COUNT(2)
    ) driver (
        .clk(clk),
        .data_clk(data_clk),
        .rst(rst),
        .en(enable_driver && init_done),
        .wr_full(wr_full),
        .wr_data(disp_wr_data),
        .wr_valid(disp_wr_valid),
        .fb_valid(fb_valid),
        .fb0_addr(fb0_addr),
        .fb1_addr(fb1_addr),
        .last_dm_status(last_dm_status),
        .current_fb(current_fb),
        .state_out(driver_state),
        .m_axis_cmd_tdata(m_axis_cmd_tdata),
        .m_axis_cmd_tvalid(m_axis_cmd_tvalid),
        .m_axis_cmd_tready(m_axis_cmd_tready),
        .s_axis_sts_tdata(s_axis_sts_tdata),
        .s_axis_sts_tkeep(s_axis_sts_tkeep),
        .s_axis_sts_tlast(s_axis_sts_tlast),
        .s_axis_sts_tvalid(s_axis_sts_tvalid),
        .s_axis_sts_tready(s_axis_sts_tready),
        .s_axis_data_tdata(s_axis_data_tdata),
        .s_axis_data_tkeep(s_axis_data_tkeep),
        .s_axis_data_tlast(s_axis_data_tlast),
        .s_axis_data_tvalid(s_axis_data_tvalid),
        .s_axis_data_tready(s_axis_data_tready),
        .datamover_mm2s_err(datamover_mm2s_err)
    );

endmodule
