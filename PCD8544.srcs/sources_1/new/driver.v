`timescale 1ns / 1ps

module driver #(
        parameter FB_COUNT = 2
    )(
        input clk,
        input data_clk,
        input rst,
        input en,

        input wr_full,
        output wire [8:0] wr_data,
        output wire wr_valid,

        input [$clog2(FB_COUNT+1)-1:0] fb_valid,
        input [31:0] fb0_addr,
        input [31:0] fb1_addr,

        output reg [7:0] last_dm_status,
        output reg [$clog2(FB_COUNT)-1:0] current_fb,
        output wire [4:0] state_out,

        // master AXI-Stream command channel TO DataMover
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_CMD TDATA"   *) output reg  [71:0] m_axis_cmd_tdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_CMD TREADY"  *) output reg         m_axis_cmd_tvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_CMD TVALID"  *) input  wire        m_axis_cmd_tready,

        // slave AXI-Stream status channel FROM DataMover
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TDATA"   *) input  wire [7:0]  s_axis_sts_tdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TKEEP"   *) input  wire [0:0]  s_axis_sts_tkeep,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TLAST"   *) input  wire        s_axis_sts_tlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TREADY"  *) input  wire        s_axis_sts_tvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_STS TVALID"  *) output reg         s_axis_sts_tready,

        // slave AXI-Stream data channel FROM DataMover
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TDATA"  *) input  wire [63:0] s_axis_data_tdata,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TKEEP"  *) input  wire [7:0]  s_axis_data_tkeep,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TLAST"  *) input  wire        s_axis_data_tlast,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TREADY" *) input  wire        s_axis_data_tvalid,
        (* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_DATA TVALID" *) output reg         s_axis_data_tready,

        // DataMover memory-mapped to stream error
        input wire datamover_mm2s_err
    );

    initial begin
        s_axis_sts_tready <= 1;
    end

    always @(posedge clk) begin
        if (rst) begin
            last_dm_status <= 0;
            s_axis_sts_tready <= 1;
        end
        else if (s_axis_sts_tvalid) begin
            if (s_axis_sts_tkeep[0])
                last_dm_status <= s_axis_sts_tdata;
        end
    end

    localparam DM_STATE_CMD   = 0;
    localparam DM_STATE_DATA  = 1;
    localparam DM_STATE_WAIT  = 2;
    localparam DM_STATE_FINAL = 3;

    localparam PCD_STATE_POSX = 0;
    localparam PCD_STATE_POSY = 1;
    localparam PCD_STATE_DATA = 2;

    localparam PIXEL_COUNT = 504; // 504 = 84 wide * 48 tall * 1 bit per pixel / 8

    reg [3:0] tag = 0;
    reg [$clog2(FB_COUNT)-1:0] fbi = 0;
    reg [2:0] keepctr = 0;
    reg [1:0] dm_state = DM_STATE_CMD;
    reg [1:0] pcd_state = PCD_STATE_POSX;

    reg [8:0] wr_data_d;
    reg wr_valid_d;

    reg [63:0] tdata;
    reg [7:0] tkeep;
    reg tlast;
    reg tvalid = 0;
    reg tready = 0;

    reg [31:0] fb_addr;

    wire [15:0] bytes_to_transfer = PIXEL_COUNT;

    always @* begin
        fb_addr = 0;
        current_fb = fbi;

        case (fbi)
            0: fb_addr = fb0_addr;
            1: fb_addr = fb1_addr;
            default: current_fb = 0;
        endcase
    end

    always @(posedge clk) begin
        if (rst) begin
            tag = 0;
            fbi = 0;
            tvalid = 0;
            dm_state = DM_STATE_CMD;

            m_axis_cmd_tvalid = 0;
            s_axis_data_tready = 0;
        end
        else begin
            case (dm_state)
                DM_STATE_CMD: begin
                    if (en && fb_valid != 0 && !m_axis_cmd_tvalid) begin
                        m_axis_cmd_tvalid = 1;
                        m_axis_cmd_tdata = {
                            4'h0, // reserved
                            tag, // tag
                            fb_addr, // source address
                            8'h0, // ignored
                            1'h1, // type: incr
                            7'h0, // ignored
                            bytes_to_transfer
                        };
                    end

                    if (m_axis_cmd_tvalid && m_axis_cmd_tready)
                        dm_state = DM_STATE_DATA;
                end

                DM_STATE_DATA: begin
                    m_axis_cmd_tvalid = 0;
                    s_axis_data_tready = 1;

                    if (s_axis_data_tvalid) begin
                        tvalid = 1;
                        tdata = s_axis_data_tdata;
                        tkeep = s_axis_data_tkeep;
                        tlast = s_axis_data_tlast;
                        dm_state = DM_STATE_WAIT;
                    end
                end

                DM_STATE_WAIT: begin
                    s_axis_data_tready = 0;

                    if (tready) begin
                        tvalid = 0;
                        if (tlast)
                            dm_state = DM_STATE_FINAL;
                        else
                            dm_state = DM_STATE_DATA;
                    end
                end

                DM_STATE_FINAL: begin
                    tag = tag + 1;
                    fbi = fbi + 1;
                    dm_state = DM_STATE_CMD;

                    if (fbi >= fb_valid)
                        fbi = 0;
                end
            endcase
        end
    end

    always @(posedge clk) begin
        if (rst) begin
            pcd_state = PCD_STATE_POSX;

            keepctr = 0;
        end
        else if (!wr_full) begin
            wr_valid_d = 0;
            wr_data_d = 9'hx;

            tready = 0;

            case (pcd_state)
                PCD_STATE_POSX: begin
                    if (en) begin
                        wr_valid_d = 1;
                        wr_data_d = 9'h080;
                        pcd_state = PCD_STATE_POSY;
                    end
                end

                PCD_STATE_POSY: begin
                    wr_valid_d = 1;
                    wr_data_d = 9'h040;
                    pcd_state = PCD_STATE_DATA;
                end

                PCD_STATE_DATA: begin
                    if (tvalid) begin
                        if (tkeep[keepctr]) begin
                            wr_valid_d = 1;
                            wr_data_d = 9'h100 | tdata[keepctr*8 +:8];
                        end

                        if (keepctr != 7)
                            keepctr = keepctr + 1;
                        else begin
                            keepctr = 0;
                            tready = 1;
                            if (tlast)
                                pcd_state = PCD_STATE_POSX;
                        end
                    end
                end
            endcase
        end
    end

    assign wr_data = wr_data_d;
    assign wr_valid = wr_valid_d;
    assign state_out = {dm_state, 1'b0, pcd_state};

endmodule
