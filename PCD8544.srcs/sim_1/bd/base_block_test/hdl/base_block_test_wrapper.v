//Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
//Date        : Tue Feb 13 21:50:16 2018
//Host        : Taniquetil running 64-bit major release  (build 9200)
//Command     : generate_target base_block_test_wrapper.bd
//Design      : base_block_test_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module base_block_test_wrapper
   (clk,
    gpio0,
    gpio1,
    gpio2,
    gpio3,
    gpio4,
    rst);
  input clk;
  inout [24:0]gpio0;
  inout [24:0]gpio1;
  inout [24:0]gpio2;
  inout [24:0]gpio3;
  inout [24:0]gpio4;
  input rst;

  wire clk;
  wire [24:0]gpio0;
  wire [24:0]gpio1;
  wire [24:0]gpio2;
  wire [24:0]gpio3;
  wire [24:0]gpio4;
  wire rst;

  base_block_test base_block_test_i
       (.clk(clk),
        .gpio0(gpio0),
        .gpio1(gpio1),
        .gpio2(gpio2),
        .gpio3(gpio3),
        .gpio4(gpio4),
        .rst(rst));
endmodule
