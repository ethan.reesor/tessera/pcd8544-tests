
################################################################
# This is a generated script based on design: base_block_test
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2017.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source base_block_test_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# comm_PCD8544, conn, main

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z020clg400-3
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name base_block_test

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set clk [ create_bd_port -dir I -type clk clk ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {10000000} \
 ] $clk
  set gpio0 [ create_bd_port -dir IO -from 24 -to 0 gpio0 ]
  set gpio1 [ create_bd_port -dir IO -from 24 -to 0 gpio1 ]
  set gpio2 [ create_bd_port -dir IO -from 24 -to 0 gpio2 ]
  set gpio3 [ create_bd_port -dir IO -from 24 -to 0 gpio3 ]
  set gpio4 [ create_bd_port -dir IO -from 24 -to 0 gpio4 ]
  set rst [ create_bd_port -dir I -type rst rst ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $rst

  # Create instance: comm_PCD8544_0, and set properties
  set block_name comm_PCD8544
  set block_cell_name comm_PCD8544_0
  if { [catch {set comm_PCD8544_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $comm_PCD8544_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.BAUD_CYCLES {5} \
 ] $comm_PCD8544_0

  # Create instance: conn_0, and set properties
  set block_name conn
  set block_cell_name conn_0
  if { [catch {set conn_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $conn_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: fifo_generator_0, and set properties
  set fifo_generator_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 fifo_generator_0 ]
  set_property -dict [ list \
   CONFIG.Data_Count {true} \
   CONFIG.Data_Count_Width {9} \
   CONFIG.Enable_Safety_Circuit {false} \
   CONFIG.FIFO_Implementation_rach {Common_Clock_Distributed_RAM} \
   CONFIG.FIFO_Implementation_wach {Common_Clock_Distributed_RAM} \
   CONFIG.FIFO_Implementation_wrch {Common_Clock_Distributed_RAM} \
   CONFIG.Fifo_Implementation {Common_Clock_Block_RAM} \
   CONFIG.Full_Flags_Reset_Value {1} \
   CONFIG.INTERFACE_TYPE {Native} \
   CONFIG.Input_Data_Width {9} \
   CONFIG.Input_Depth {512} \
   CONFIG.Output_Data_Width {9} \
   CONFIG.Overflow_Flag {false} \
   CONFIG.Reset_Type {Asynchronous_Reset} \
   CONFIG.Use_Dout_Reset {true} \
   CONFIG.Valid_Flag {true} \
   CONFIG.Write_Acknowledge_Flag {false} \
 ] $fifo_generator_0

  # Create instance: main_0, and set properties
  set block_name main
  set block_cell_name main_0
  if { [catch {set main_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $main_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.CLK_FREQ {10000000} \
 ] $main_0

  # Create interface connections
  connect_bd_intf_net -intf_net comm_PCD8544_0_FIFO_READ [get_bd_intf_pins comm_PCD8544_0/DIN] [get_bd_intf_pins fifo_generator_0/FIFO_READ]
  connect_bd_intf_net -intf_net main_0_FIFO_WRITE [get_bd_intf_pins fifo_generator_0/FIFO_WRITE] [get_bd_intf_pins main_0/DOUT]

  # Create port connections
  connect_bd_net -net Net [get_bd_ports gpio0] [get_bd_pins conn_0/gpio0]
  connect_bd_net -net Net1 [get_bd_ports gpio1] [get_bd_pins conn_0/gpio1]
  connect_bd_net -net Net2 [get_bd_ports gpio2] [get_bd_pins conn_0/gpio2]
  connect_bd_net -net Net3 [get_bd_ports gpio3] [get_bd_pins conn_0/gpio3]
  connect_bd_net -net Net4 [get_bd_ports gpio4] [get_bd_pins conn_0/gpio4]
  connect_bd_net -net clockgen_0_clk [get_bd_ports clk] [get_bd_pins comm_PCD8544_0/clk] [get_bd_pins conn_0/clk] [get_bd_pins fifo_generator_0/clk] [get_bd_pins main_0/clk]
  connect_bd_net -net clockgen_0_rst [get_bd_ports rst] [get_bd_pins comm_PCD8544_0/rst] [get_bd_pins fifo_generator_0/rst] [get_bd_pins main_0/rst]
  connect_bd_net -net comm_PCD8544_0_dc [get_bd_pins comm_PCD8544_0/dc] [get_bd_pins conn_0/comm_dc]
  connect_bd_net -net comm_PCD8544_0_mosi [get_bd_pins comm_PCD8544_0/mosi] [get_bd_pins conn_0/comm_mosi]
  connect_bd_net -net comm_PCD8544_0_res [get_bd_pins comm_PCD8544_0/res] [get_bd_pins conn_0/comm_res]
  connect_bd_net -net comm_PCD8544_0_sce [get_bd_pins comm_PCD8544_0/sce] [get_bd_pins conn_0/comm_sce]
  connect_bd_net -net comm_PCD8544_0_sclk [get_bd_pins comm_PCD8544_0/sclk] [get_bd_pins conn_0/comm_sclk]
  connect_bd_net -net fifo_generator_0_valid [get_bd_pins comm_PCD8544_0/rd_valid] [get_bd_pins fifo_generator_0/valid]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


