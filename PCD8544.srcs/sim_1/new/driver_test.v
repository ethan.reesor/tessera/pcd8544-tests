`timescale 1ns / 1ps

module driver_test();

    reg clk = 0;
    reg rst = 0;

    // reg [31:0] tdata;
    reg tlast;
    reg [8:0] idata;
    wire tready;

    initial
    begin
        rst = 1;
        #200 rst = 0;
    end

    always
    begin
        #100 clk = ~clk;
    end

    always @(posedge clk) begin
        if (rst) begin
            // tdata = 0;
            tlast = 0;
            idata = 0;
        end
        else if (tready) begin
            if (idata == 503) begin
                tlast = 1;
                idata = 0;
            end
            else begin
                tlast = 0;
                idata = idata + 1;
            end
        end
    end

    driver driver(
        .clk(clk),
        .data_clk(clk),
        .rst(rst),
        .en(1),

        .wr_full(0),

        .fb_valid(2),
        .fb0_addr(1),
        .fb1_addr(2),

        .m_axis_cmd_tready(1),

        // slave AXI-Stream status channel FROM DataMover
        .s_axis_sts_tdata(8'h80),
        .s_axis_sts_tkeep(1),
        .s_axis_sts_tlast(1),
        .s_axis_sts_tvalid(1),

        // slave AXI-Stream data channel FROM DataMover
        .s_axis_data_tdata({1'h0, idata, idata, idata, idata, idata, idata, idata}),
        .s_axis_data_tkeep(8'hFF),
        .s_axis_data_tlast(tlast),
        .s_axis_data_tvalid(1),
        .s_axis_data_tready(tready),
        .datamover_mm2s_err(0)
    );

endmodule
