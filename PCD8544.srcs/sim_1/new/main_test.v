`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/12/2018 10:59:54 PM
// Design Name: 
// Module Name: main_test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_test();

    reg clk = 0;
    reg rst = 0;

    initial
    begin
        rst = 1;
        #200 rst = 0;
    end

    always
    begin
        #100 clk = ~clk;
    end
    
    base_block_test_wrapper block(
        .clk(clk),
        .rst(rst)
    );

endmodule
