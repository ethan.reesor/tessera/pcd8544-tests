###############################################################################
#
#   Constraints file for snickerdoodle black
#
#   Copyright (c) 2016 krtkl inc.
#
###############################################################################
#
#------------------------------------------------------------------------------
# Constraints for GPIO outputs
#------------------------------------------------------------------------------
# JA1 Connector
#------------------------------------------------------------------------------
### JA1.4 (IO_0_35)
set_property PACKAGE_PIN    G14         [get_ports {gpio0[24]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[24]}]

### JA1.5 (IO_L5P_T0_AD9P_35)
set_property PACKAGE_PIN    E18         [get_ports {gpio0[8]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[8]}]

### JA1.6 (IO_L4N_T0_35)
set_property PACKAGE_PIN    D20         [get_ports {gpio0[11]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[11]}]

### JA1.7 (IO_L5N_T0_AD9N_35)
set_property PACKAGE_PIN    E19         [get_ports {gpio0[9]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[9]}]

### JA1.8 (IO_L4P_T0_35)
set_property PACKAGE_PIN    D19         [get_ports {gpio0[10]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[10]}]

### JA1.11 (IO_L6P_T0_35)
set_property PACKAGE_PIN    F16         [get_ports {gpio0[12]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[12]}]

### JA1.12 (IO_L1N_T0_AD0N_35)
set_property PACKAGE_PIN    B20         [get_ports {gpio0[15]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[15]}]

### JA1.13 (IO_L6N_T0_VREF_35)
set_property PACKAGE_PIN    F17         [get_ports {gpio0[13]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[13]}]

### JA1.14 (IO_L1P_T0_AD0P_35)
set_property PACKAGE_PIN    C20         [get_ports {gpio0[14]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[14]}]

### JA1.17 (IO_L3P_T0_DQS_AD1P_35)
set_property PACKAGE_PIN    E17         [get_ports {gpio0[20]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[20]}]

### JA1.18 (IO_L2N_T0_AD8N_35)
set_property PACKAGE_PIN    A20         [get_ports {gpio0[17]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[17]}]

### JA1.19 (IO_L3N_T0_DQS_AD1N_35)
set_property PACKAGE_PIN    D18         [get_ports {gpio0[21]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[21]}]

### JA1.20 (IO_L2P_T0_AD8P_35)
set_property PACKAGE_PIN    B19         [get_ports {gpio0[16]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[16]}]

### JA1.23 (IO_L15P_T2_DQS_AD12P_35)
set_property PACKAGE_PIN    F19         [get_ports {gpio0[18]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[18]}]

### JA1.24 (IO_L18N_T2_AD13N_35)
set_property PACKAGE_PIN    G20         [get_ports {gpio0[1]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[1]}]

### JA1.25 (IO_L15N_T2_DQS_AD12N_35)
set_property PACKAGE_PIN    F20         [get_ports {gpio0[19]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[19]}]

### JA1.26 (IO_L18P_T2_AD13P_35)
set_property PACKAGE_PIN    G19         [get_ports {gpio0[0]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[0]}]

### JA1.29 (IO_L17P_T2_AD5P_35)
set_property PACKAGE_PIN    J20         [get_ports {gpio0[2]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[2]}]

### JA1.30 (IO_L16N_T2_35)
set_property PACKAGE_PIN    G18         [get_ports {gpio0[5]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[5]}]

### JA1.31 (IO_L17N_T2_AD5N_35)
set_property PACKAGE_PIN    H20         [get_ports {gpio0[3]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[3]}]

### JA1.32 (IO_L16P_T2_35)
set_property PACKAGE_PIN    G17         [get_ports {gpio0[4]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[4]}]

### JA1.35 (IO_L14P_T2_AD4P_SRCC_35)
set_property PACKAGE_PIN    J18         [get_ports {gpio0[6]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[6]}]

### JA1.36 (IO_L13N_T2_MRCC_35)
set_property PACKAGE_PIN    H17         [get_ports {gpio0[23]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[23]}]

### JA1.37 (IO_L14N_T2_AD4N_SRCC_35)
set_property PACKAGE_PIN    H18         [get_ports {gpio0[7]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[7]}]

### JA1.38 (IO_L13P_T2_MRCC_35)
set_property PACKAGE_PIN    H16         [get_ports {gpio0[22]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio0[22]}]

#------------------------------------------------------------------------------
# JA2 Connector
#------------------------------------------------------------------------------
### JA2.4 (IO_25_35)
set_property PACKAGE_PIN    J15         [get_ports {gpio1[24]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[24]}]

### JA2.5 (IO_L22P_T3_AD7P_35)
set_property PACKAGE_PIN    L14         [get_ports {gpio1[8]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[8]}]

### JA2.6 (IO_L24N_T3_AD15N_35)
set_property PACKAGE_PIN    J16         [get_ports {gpio1[11]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[11]}]

### JA2.7 (IO_L22N_T3_AD7N_35)
set_property PACKAGE_PIN    L15         [get_ports {gpio1[9]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[9]}]

### JA2.8 (IO_L24P_T3_AD15P_35)
set_property PACKAGE_PIN    K16         [get_ports {gpio1[10]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[10]}]

### JA2.11 (IO_L23P_T3_35)
set_property PACKAGE_PIN    M14         [get_ports {gpio1[12]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[12]}]

### JA2.12 (IO_L19N_T3_VREF_35)
set_property PACKAGE_PIN    G15         [get_ports {gpio1[15]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[15]}]

### JA2.13 (IO_L23N_T3_35)
set_property PACKAGE_PIN    M15         [get_ports {gpio1[13]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[13]}]

### JA2.14 (IO_L19P_T3_35)
set_property PACKAGE_PIN    H15         [get_ports {gpio1[14]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[14]}]

### JA2.17 (IO_L21P_T3_DQS_AD14P_35)
set_property PACKAGE_PIN    N15         [get_ports {gpio1[20]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[20]}]

### JA2.18 (IO_L20N_T3_AD6N_35)
set_property PACKAGE_PIN    J14         [get_ports {gpio1[17]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[17]}]

### JA2.19 (IO_L21N_T3_DQS_AD14N_35)
set_property PACKAGE_PIN    N16         [get_ports {gpio1[21]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[21]}]

### JA2.20 (IO_L20P_T3_AD6P_35)
set_property PACKAGE_PIN    K14         [get_ports {gpio1[16]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[16]}]

### JA2.23 (IO_L9P_T1_DQS_AD3P_35)
set_property PACKAGE_PIN    L19         [get_ports {gpio1[18]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[18]}]

### JA2.24 (IO_L10N_T1_AD11N_35)
set_property PACKAGE_PIN    J19         [get_ports {gpio1[1]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[1]}]

### JA2.25 (IO_L9N_T1_DQS_AD3N_35)
set_property PACKAGE_PIN    L20         [get_ports {gpio1[19]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[19]}]

### JA2.26 (IO_L10P_T1_AD11P_35)
set_property PACKAGE_PIN    K19         [get_ports {gpio1[0]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[0]}]

### JA2.29 (IO_L8P_T1_AD10P_35)
set_property PACKAGE_PIN    M17         [get_ports {gpio1[2]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[2]}]

### JA2.30 (IO_L7N_T1_AD2N_35)
set_property PACKAGE_PIN    M20         [get_ports {gpio1[5]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[5]}]

### JA2.31 (IO_L8N_T1_AD10N_35)
set_property PACKAGE_PIN    M18         [get_ports {gpio1[3]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[3]}]

### JA2.32 (IO_L7P_T1_AD2P_35)
set_property PACKAGE_PIN    M19         [get_ports {gpio1[4]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[4]}]

### JA2.35 (IO_L11P_T1_SRCC_35)
set_property PACKAGE_PIN    L16         [get_ports {gpio1[6]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[6]}]

### JA2.36 (IO_L12N_T1_MRCC_35)
set_property PACKAGE_PIN    K18         [get_ports {gpio1[23]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[23]}]

### JA2.37 (IO_L11N_T1_SRCC_35)
set_property PACKAGE_PIN    L17         [get_ports {gpio1[7]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[7]}]

### JA2.38 (IO_L12P_T1_MRCC_35)
set_property PACKAGE_PIN    K17         [get_ports {gpio1[22]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio1[22]}]

#------------------------------------------------------------------------------
# JB1 Connector
#------------------------------------------------------------------------------
### JB1.4 (IO_25_34)
set_property PACKAGE_PIN    T19         [get_ports {gpio2[24]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[24]}]

### JB1.5 (IO_L1P_T0_34)
set_property PACKAGE_PIN    T11         [get_ports {gpio2[8]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[8]}]

### JB1.6 (IO_L2N_T0_34)
set_property PACKAGE_PIN    U12         [get_ports {gpio2[11]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[11]}]

### JB1.7 (IO_L1N_T0_34)
set_property PACKAGE_PIN    T10         [get_ports {gpio2[9]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[9]}]

### JB1.8 (IO_L2P_T0_34)
set_property PACKAGE_PIN    T12         [get_ports {gpio2[10]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[10]}]

### JB1.11 (IO_L6P_T0_34)
set_property PACKAGE_PIN    P14         [get_ports {gpio2[12]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[12]}]

### JB1.12 (IO_L4N_T0_34)
set_property PACKAGE_PIN    W13         [get_ports {gpio2[15]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[15]}]

### JB1.13 (IO_L6N_T0_VREF_34)
set_property PACKAGE_PIN    R14         [get_ports {gpio2[13]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[13]}]

### JB1.14 (IO_L4P_T0_34)
set_property PACKAGE_PIN    V12         [get_ports {gpio2[14]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[14]}]

### JB1.17 (IO_L3P_T0_DQS_PUDC_B_34)
set_property PACKAGE_PIN    U13         [get_ports {gpio2[20]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[20]}]

### JB1.18 (IO_L5N_T0_34)
set_property PACKAGE_PIN    T15         [get_ports {gpio2[17]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[17]}]

### JB1.19 (IO_L3N_T0_DQS_34)
set_property PACKAGE_PIN    V13         [get_ports {gpio2[21]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[21]}]

### JB1.20 (IO_L5P_T0_34)
set_property PACKAGE_PIN    T14         [get_ports {gpio2[16]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[16]}]

### JB1.23 (IO_L9P_T1_DQS_34)
set_property PACKAGE_PIN    T16         [get_ports {gpio2[18]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[18]}]

### JB1.24 (IO_L7N_T1_34)
set_property PACKAGE_PIN    Y17         [get_ports {gpio2[1]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[1]}]

### JB1.25 (IO_L9N_T1_DQS_34)
set_property PACKAGE_PIN    U17         [get_ports {gpio2[19]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[19]}]

### JB1.26 (IO_L7P_T1_34)
set_property PACKAGE_PIN    Y16         [get_ports {gpio2[0]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[0]}]

### JB1.29 (IO_L8P_T1_34)
set_property PACKAGE_PIN    W14         [get_ports {gpio2[2]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[2]}]

### JB1.30 (IO_L10N_T1_34)
set_property PACKAGE_PIN    W15         [get_ports {gpio2[5]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[5]}]

### JB1.31 (IO_L8N_T1_34)
set_property PACKAGE_PIN    Y14         [get_ports {gpio2[3]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[3]}]

### JB1.32 (IO_L10P_T1_34)
set_property PACKAGE_PIN    V15         [get_ports {gpio2[4]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[4]}]

### JB1.35 (IO_L11P_T1_SRCC_34)
set_property PACKAGE_PIN    U14         [get_ports {gpio2[6]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[6]}]

### JB1.36 (IO_L12N_T1_MRCC_34)
set_property PACKAGE_PIN    U19         [get_ports {gpio2[23]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[23]}]

### JB1.37 (IO_L11N_T1_SRCC_34)
set_property PACKAGE_PIN    U15         [get_ports {gpio2[7]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[7]}]

### JB1.38 (IO_L12P_T1_MRCC_34)
set_property PACKAGE_PIN    U18         [get_ports {gpio2[22]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio2[22]}]

#------------------------------------------------------------------------------
# JB2 Connector
#------------------------------------------------------------------------------
### JB2.4 (IO_0_34)
set_property PACKAGE_PIN    R19         [get_ports {gpio3[24]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[24]}]

### JB2.5 (IO_L23P_T3_34)
set_property PACKAGE_PIN    N17         [get_ports {gpio3[8]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[8]}]

### JB2.6 (IO_L24N_T3_34)
set_property PACKAGE_PIN    P16         [get_ports {gpio3[11]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[11]}]

### JB2.7 (IO_L23N_T3_34)
set_property PACKAGE_PIN    P18         [get_ports {gpio3[9]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[9]}]

### JB2.8 (IO_L24P_T3_34)
set_property PACKAGE_PIN    P15         [get_ports {gpio3[10]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[10]}]

### JB2.11 (IO_L20P_T3_34)
set_property PACKAGE_PIN    T17         [get_ports {gpio3[12]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[12]}]

### JB2.12 (IO_L19N_T3_VREF_34)
set_property PACKAGE_PIN    R17         [get_ports {gpio3[15]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[15]}]

### JB2.13 (IO_L20N_T3_34)
set_property PACKAGE_PIN    R18         [get_ports {gpio3[13]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[13]}]

### JB2.14 (IO_L19P_T3_34)
set_property PACKAGE_PIN    R16         [get_ports {gpio3[14]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[14]}]

### JB2.17 (IO_L21P_T3_DQS_34)
set_property PACKAGE_PIN    V17         [get_ports {gpio3[20]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[20]}]

### JB2.18 (IO_L22N_T3_34)
set_property PACKAGE_PIN    W19         [get_ports {gpio3[17]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[17]}]

### JB2.19 (IO_L21N_T3_DQS_34)
set_property PACKAGE_PIN    V18         [get_ports {gpio3[21]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[21]}]

### JB2.20 (IO_L22P_T3_34)
set_property PACKAGE_PIN    W18         [get_ports {gpio3[16]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[16]}]

### JB2.23 (IO_L15P_T2_DQS_34)
set_property PACKAGE_PIN    T20         [get_ports {gpio3[18]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[18]}]

### JB2.24 (IO_L18N_T2_34)
set_property PACKAGE_PIN    W16         [get_ports {gpio3[1]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[1]}]

### JB2.25 (IO_L15N_T2_DQS_34)
set_property PACKAGE_PIN    U20         [get_ports {gpio3[19]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[19]}]

### JB2.26 (IO_L18P_T2_34)
set_property PACKAGE_PIN    V16         [get_ports {gpio3[0]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[0]}]

### JB2.29 (IO_L16P_T2_34)
set_property PACKAGE_PIN    V20         [get_ports {gpio3[2]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[2]}]

### JB2.30 (IO_L17N_T2_34)
set_property PACKAGE_PIN    Y19         [get_ports {gpio3[5]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[5]}]

### JB2.31 (IO_L16N_T2_34)
set_property PACKAGE_PIN    W20         [get_ports {gpio3[3]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[3]}]

### JB2.32 (IO_L17P_T2_34)
set_property PACKAGE_PIN    Y18         [get_ports {gpio3[4]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[4]}]

### JB2.35 (IO_L14P_T2_SRCC_34)
set_property PACKAGE_PIN    N20         [get_ports {gpio3[6]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[6]}]

### JB2.36 (IO_L13N_T2_MRCC_34)
set_property PACKAGE_PIN    P19         [get_ports {gpio3[23]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[23]}]

### JB2.37 (IO_L14N_T2_SRCC_34)
set_property PACKAGE_PIN    P20         [get_ports {gpio3[7]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[7]}]

### JB2.38 (IO_L13P_T2_MRCC_34)
set_property PACKAGE_PIN    N18         [get_ports {gpio3[22]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio3[22]}]

##------------------------------------------------------------------------------
## JC1 Connector
##------------------------------------------------------------------------------
### JC1.4 (IO_L6N_T0_VREF_13)
set_property PACKAGE_PIN    V5          [get_ports {gpio4[24]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[24]}]

### JC1.5 (IO_L11P_T1_SRCC_13)
set_property PACKAGE_PIN    U7          [get_ports {gpio4[8]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[8]}]

### JC1.6 (IO_L12N_T1_MRCC_13)
set_property PACKAGE_PIN    U10         [get_ports {gpio4[11]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[11]}]

### JC1.7 (IO_L11N_T1_SRCC_13)
set_property PACKAGE_PIN    V7          [get_ports {gpio4[9]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[9]}]

### JC1.8 (IO_L12P_T1_MRCC_13)
set_property PACKAGE_PIN    T9          [get_ports {gpio4[10]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[10]}]

### JC1.11 (IO_L19P_T3_13)
set_property PACKAGE_PIN    T5          [get_ports {gpio4[12]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[12]}]

### JC1.12 (IO_L20N_T3_13)
set_property PACKAGE_PIN    Y13         [get_ports {gpio4[15]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[15]}]

### JC1.13 (IO_L19N_T3_VREF_13)
set_property PACKAGE_PIN    U5          [get_ports {gpio4[13]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[13]}]

### JC1.14 (IO_L20P_T3_13)
set_property PACKAGE_PIN    Y12         [get_ports {gpio4[14]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[14]}]

### JC1.17 (IO_L21P_T3_DQS_13)
set_property PACKAGE_PIN    V11         [get_ports {gpio4[20]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[20]}]

### JC1.18 (IO_L22N_T3_13)
set_property PACKAGE_PIN    W6          [get_ports {gpio4[17]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[17]}]

### JC1.19 (IO_L21N_T3_DQS_13)
set_property PACKAGE_PIN    V10         [get_ports {gpio4[21]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[21]}]

### JC1.20 (IO_L22P_T3_13)
set_property PACKAGE_PIN    V6          [get_ports {gpio4[16]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[16]}]

### JC1.23 (IO_L15P_T2_DQS_13)
set_property PACKAGE_PIN    V8          [get_ports {gpio4[18]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[18]}]

### JC1.24 (IO_L18N_T2_13)
set_property PACKAGE_PIN    Y11         [get_ports {gpio4[1]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[1]}]

### JC1.25 (IO_L15N_T2_DQS_13)
set_property PACKAGE_PIN    W8          [get_ports {gpio4[19]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[19]}]

### JC1.26 (IO_L18P_T2_13)
set_property PACKAGE_PIN    W11         [get_ports {gpio4[0]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[0]}]

### JC1.29 (IO_L17P_T2_13)
set_property PACKAGE_PIN    U9          [get_ports {gpio4[2]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[2]}]

### JC1.30 (IO_L16N_T2_13)
set_property PACKAGE_PIN    W9          [get_ports {gpio4[5]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[5]}]

### JC1.31 (IO_L17N_T2_13)
set_property PACKAGE_PIN    U8          [get_ports {gpio4[3]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[3]}]

### JC1.32 (IO_L16P_T2_13)
set_property PACKAGE_PIN    W10         [get_ports {gpio4[4]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[4]}]

### JC1.35 (IO_L14P_T2_SRCC_13)
set_property PACKAGE_PIN    Y9          [get_ports {gpio4[6]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[6]}]

### JC1.36 (IO_L13N_T2_MRCC_13)
set_property PACKAGE_PIN    Y6          [get_ports {gpio4[23]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[23]}]

### JC1.37 (IO_L14N_T2_SRCC_13)
set_property PACKAGE_PIN    Y8          [get_ports {gpio4[7]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[7]}]

### JC1.38 (IO_L13P_T2_MRCC_13)
set_property PACKAGE_PIN    Y7          [get_ports {gpio4[22]}]
set_property IOSTANDARD     LVCMOS33    [get_ports {gpio4[22]}]
